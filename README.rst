CA1
===

Modify this file to complete the following questions.
Other than filling in required information, please *DO NOT* make any changes to this file's structure or the question text (to ease grading).

Student information
-------------------

Name (as on Moodle): David McQuaid

Sortable name (no spaces, surname then firstname): mcquaid_david

Reflection questions (after coding)
-----------------------------------

When you have finished coding, please reflect briefly on the following three questions. Marks here are awarded for engaging with the question - they're not a trick, and there is no "right" answer.

Question 1
^^^^^^^^^^

If you had much more time to work on this problem, how would you attempt to improve your code? (Suggested length: one short paragraph)

YOUR ANSWERE HERE.
I would tidy up all the functions and remove the globals. The detector needs a lot more work and I would probably use wavelets and some type of filter to smooth out the plot. I would also include some file input validation.


Question 2
^^^^^^^^^^

What is the most important thing that you learned from this lab exercise? (Suggested length: one sentence)


YOUR ANSWERE HERE.
Trying to get a cyclical read on any type of data is not so simple!


Question 3
^^^^^^^^^^

What did you like/dislike the most about this lab exercise? (Suggested length: one sentence)

YOUR ANSWER HERE.
I really enjoyed working with python and the use of automation.

