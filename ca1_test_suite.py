"""
CA1 ECG analysis

Student name: David McQuaid
"""
import unittest
#from ca1_analysis import handleannotations

time=[137.00,138.11,139.23]
ecg1=[0.23,0.58,1.36]
ecg2=[4.25,0.25,1.36]
headings=["a","b","c","d","e","f"]
checker=[]
annotations=["g","h","i"]
annoinfo=[]

def printoutput():

#getting start time
 start = time[0]
#getting end time
 end = time[-1]
#getting max signal in ecg1 and returning it as a string
 msig1 = repr(max(ecg1))
#getting max signal in ecg2 and returning it as a string
 msig2 = repr(max(ecg2))
#getting min signal in ecg1 and returning it as a string
 minsig1 = repr(min(ecg1))
#getting min signal in ecg2 and returning it as a string
 minsig2 = repr(min(ecg2))
#working out the total time by taking the end time from the start time
 totaltime = end - start
#rounding the time off to the nearest second
 totaltime = round(totaltime,0)
#turning the total time into an int to remove the decimal zero
 totaltime = int(totaltime)
#getting the total number of samples by returning the length of one 
#of the sample lists (time, ecg1 and ecg2 are all the same size so 
#the choice is arbitary)
 datalen = len(ecg1)
#working out the sample rate by dividing the total number of samples by
#the totaltime in seconds to give a value in Hz
 signalrate = datalen/totaltime
#rounding the time off to the nearest Hz
 signalrate = round(signalrate,0)
#turning the signal rate into an int to remove the decimal zero
 signalrate = int(signalrate)
#number of R waves in annotation file equals the length 
 numR = len(annotations)


 print("sampling rate: " + repr(signalrate) + " Hz")
 print("data length: " + repr(datalen) + " samples")
 print("time length: " + (repr(totaltime) + " " + headings[3].strip("'")))

 if msig1>msig2:
  print("max signal value: " + msig1 + " " + headings[5].strip("'"))
 else:
  print("max signal value: = " + msig2 + headings[5].strip("'"))
 
 if minsig1<minsig2:
  print("min signal value: " + minsig1 + " " + headings[5].strip("'"))
 else:
  print("min signal value: = " + minsig2 + headings[5].strip("'"))
  
 print("number of R waves in annotation file: " + repr(numR))

def r_wave_detector():
#only want the max signals that each wave generates
#therefor create a limit that states only signals above this are R waves
#Using the max signal we use 2/3 value of max as our cut off
 cut_sig=2*(max(ecg1)/3)
 sig_count=0
 sig_time=0
 for s in ecg1:
     sig_time+=1
     if s>cut_sig:
         sig_count+=1
         #plt.plot(time[sig_time],s,"o", markersize=5, color="y")
 phan_add=sig_count-len(annotations)
 percent_add=(phan_add*len(annotations)/100)       
 print("phantom beats added: " + repr(int(phan_add)) + " ("+ repr(round(percent_add)) +"%)")
 

 cut_sig2=(max(ecg1)/2)
 sig_count2=0
 sig_time2=0
 for s in ecg1:
     sig_time2+=1
     if s>cut_sig2 and s<cut_sig:
         sig_count2 +=1
         #plt.plot(time[sig_time2],s,"o", markersize=5, color="g")
 avg_miss=sig_count2/2
 percent_miss=(avg_miss*len(annotations)/100)
     
 print("actual beats missed: " + repr(int(avg_miss)) + " ("+ repr(round(percent_miss)) +"%)")
 
def handleannotations():
 for t in annotations:
    atitle=headings[-1]
    aresult=t
    areslist=aresult.split(" ")
    atime=areslist[0]
    annopoininfo=[]
    annopoininfo.append(atitle)
    annopoininfo.append(aresult)
    annopoininfo.append(atime)
    annoinfo.append(annopoininfo)
 
class ca1_ecg_tests(unittest.TestCase):
     

    def test_printout(self):
        self.assertRaises(printoutput())
    def test_detector(self):
        self.assertRaises(r_wave_detector())
    def test_annotations(self):
        self.assertRaises(handleannotations()) 
    def test_annosize(self):
        self.assertEqual(len(annotations), 3)
        print("TRUE")
        
 
if __name__ == '__main__':
    unittest.main() 