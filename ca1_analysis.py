"""
CA1 ECG analysis

Student name: David McQuaid
"""

import sys
# imports go here (anything in numpy/scipy is fine)
import matplotlib.pyplot as plt
# re-usable functions / classes go here

time=[]
ecg1=[]
ecg2=[]
headings=[]
checker=[]
annotations=[]
annoinfo=[]
annotation_Rpoints=[]
infiles=sys.argv[1]




# Read and process files

def readInputFiles(fileName1,fileName2):
 
 with open(fileName1,"r") as rIF:
   for data in rIF:  
       data=data.strip()
       splitdata=data.split(",")
       

       for chk in splitdata:
           value=None
       try:
            value = float(chk)
       except ValueError:
            for val in splitdata:
             headings.append(val)
             checker.append(value)

       else:
           time.append(float(splitdata[0]))
           ecg1.append(float(splitdata[1]))
           ecg2.append(float(splitdata[2]))
 
 with open(fileName2,"r") as rIF2:
   for data2 in rIF2:  
       #data2=' '.join(data2.split())
       data2=data2.strip()
       data2=data2.replace("\t"," ")
       annotations.append(data2)
       
 data3=annotations[0]
 annotations.remove(data3)
 data3=repr(data3)
 headings.append(data3)

def handleannotations():
 for t in annotations:
    atitle=headings[-1]
    aresult=t
    areslist=aresult.split(" ")
    atime=areslist[0]
    annopoininfo=[]
    annopoininfo.append(atitle)
    annopoininfo.append(aresult)
    annopoininfo.append(atime)
    annoinfo.append(annopoininfo)
    

def printoutput():

#getting start time
 start = time[0]
#getting end time
 end = time[-1]
#getting max signal in ecg1 and returning it as a string
 msig1 = repr(max(ecg1))
#getting max signal in ecg2 and returning it as a string
 msig2 = repr(max(ecg2))
#getting min signal in ecg1 and returning it as a string
 minsig1 = repr(min(ecg1))
#getting min signal in ecg2 and returning it as a string
 minsig2 = repr(min(ecg2))
#working out the total time by taking the end time from the start time
 totaltime = end - start
#rounding the time off to the nearest second
 totaltime = round(totaltime,0)
#turning the total time into an int to remove the decimal zero
 totaltime = int(totaltime)
#getting the total number of samples by returning the length of one 
#of the sample lists (time, ecg1 and ecg2 are all the same size so 
#the choice is arbitary)
 datalen = len(ecg1)
#working out the sample rate by dividing the total number of samples by
#the totaltime in seconds to give a value in Hz
 signalrate = datalen/totaltime
#rounding the time off to the nearest Hz
 signalrate = round(signalrate,0)
#turning the signal rate into an int to remove the decimal zero
 signalrate = int(signalrate)
#number of R waves in annotation file equals the length 
 numR = len(annotations)


 print("sampling rate: " + repr(signalrate) + " Hz")
 print("data length: " + repr(datalen) + " samples")
 print("time length: " + (repr(totaltime) + " " + headings[3].strip("'")))

 if msig1>msig2:
  print("max signal value: " + msig1 + " " + headings[5].strip("'"))
 else:
  print("max signal value: = " + msig2 + headings[5].strip("'"))
 
 if minsig1<minsig2:
  print("min signal value: " + minsig1 + " " + headings[5].strip("'"))
 else:
  print("min signal value: = " + minsig2 + headings[5].strip("'"))
  
 print("number of R waves in annotation file: " + repr(numR))

#to change the visability of the Rwave annotations on mouseover 
def on_move(event):
    visibility_changed = False
    for point, annotation in annotation_Rpoints:
        should_be_visible = (point.contains(event)[0] == True)

        if should_be_visible != annotation.get_visible():
            visibility_changed = True
            annotation.set_visible(should_be_visible)

    if visibility_changed:        
        plt.draw()

 

def plotsample():
 fig = plt.figure()
 ax = plt.axes()
 r_wave_detector()
 incount=0
#giving the plot a title
 plt.title("ECG Plot of " + infiles + " files")
#y axis label
 plt.ylabel(headings[5].strip("'"))
#x axis label
 plt.xlabel(headings[0].strip("'") + " (" + headings[3].strip("'") +")")
#plotting the samples and giving them labels
 plt.plot(time,ecg1, label=headings[1].strip("'"))
 plt.plot(time,ecg2, label=headings[2].strip("'"))
 #empty plot for simple custom legend label
 plt.plot([],'o', markersize=5, color='r', label="R Wave Data\n(Hover over point for data)")
 plt.plot([],'o', markersize=5, color='y', label="R Wave Detected By Program")
 plt.plot([],'o', markersize=5, color='g', label="Possible R wave missed by program")
#plotting the graph
 for a in annotations:
   y_anno_point = (ecg2[incount]+ecg1[incount])
   point, = plt.plot(annoinfo[incount][-1], y_anno_point, "o", markersize=5, color="r")
   annotation = ax.annotate("R Wave " + repr(incount) + " Data\n" + annoinfo[incount][0].strip("'") +"\n" + annoinfo[incount][1],
        xy=(annoinfo[incount][-1], y_anno_point), xycoords="data",ha="center",
        xytext=(annoinfo[incount][-1],y_anno_point), 
                textcoords="data",
                horizontalalignment="left",
                arrowprops=dict(arrowstyle="simple"),
                bbox=dict(boxstyle="round", 
                facecolor="y",edgecolor="0.5", alpha=0.9))      
# by default, disable the annotation visibility
   annotation.set_visible(False)

   annotation_Rpoints.append([point, annotation])
   fig.canvas.mpl_connect("motion_notify_event", on_move)
 
  #plt.annotate(annoinfo[incount][0].strip("'"), xy=(annoinfo[incount][-1], ecg1[incount]), xytext=(annoinfo[incount][-1],1))
  #plt.annotate(annoinfo[incount][1], xy=(annoinfo[incount][-1], ecg1[incount]), xytext=(annoinfo[incount][-1],0.8))#,
    #arrowprops=dict(arrowstyle="->"))
  
   incount +=1
 #plt.xlim(130.25,130.55)
 #legend position and giving it a shadow
 lege = plt.legend(loc="upper right", shadow=True, numpoints = 1)
#legend frame 
 frame = lege.get_frame()
#legend frame color between 0 and 1. 0 is darkest
 frame.set_facecolor("0.75")
 for label in lege.get_lines():
#the legend line width
    label.set_linewidth(1.5)
 plt.show()                   

def r_wave_detector():
#only want the max signals that each wave generates
#therefor create a limit that states only signals above this are R waves
#Using the max signal we use 2/3 value of max as our cut off
 cut_sig=2*(max(ecg1)/3)
 sig_count=0
 sig_time=0
 for s in ecg1:
     sig_time+=1
     if s>cut_sig:
         sig_count+=1
         plt.plot(time[sig_time],s,"o", markersize=5, color="y")
 phan_add=sig_count-len(annotations)
 percent_add=(phan_add*len(annotations)/100)       
 print("phantom beats added: " + repr(int(phan_add)) + " ("+ repr(round(percent_add)) +"%)")
 

 cut_sig2=(max(ecg1)/2)
 sig_count2=0
 sig_time2=0
 for s in ecg1:
     sig_time2+=1
     if s>cut_sig2 and s<cut_sig:
         sig_count2 +=1
         plt.plot(time[sig_time2],s,"o", markersize=5, color="g")
 avg_miss=sig_count2/2
 percent_miss=(avg_miss*len(annotations)/100)
     
 print("actual beats missed: " + repr(int(avg_miss)) + " ("+ repr(round(percent_miss)) +"%)")

           
           
           

def analyse_ecg(signal_filename, annotations_filename):
    # basic formatted output in Python
      
    print('signal filename: %s' % signal_filename)
    print('annotation filename: %s' % annotations_filename)
    readInputFiles(signal_filename, annotations_filename)     
    handleannotations()
    printoutput()
    plotsample()
    
    # your analysis code here    

if __name__=='__main__':
    analyse_ecg(sys.argv[1] + '_signals.txt', sys.argv[1]+'_annotations.txt')
    
